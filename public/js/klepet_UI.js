var kanal;
var ime;

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function divElementEnostavniHtlmTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function mizik(sporocilo){
  var smiley_array = [':)', ';)', ':*', '(y)', ':('];
  var smiley_links = ['<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">'];
    
    for(var i = 0; i < smiley_array.length; i++){
      var smiley = smiley_array[i].replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
      smiley = new RegExp(smiley, 'gi');
      sporocilo = sporocilo.replace(smiley, smiley_links[i]);
    }
    return sporocilo;
    
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
    
  } else {
        sporocilo = mizik(sporocilo);
    klepetApp.posljiSporocilo(kanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

function urediKanal(){
  $('#kanal').text(ime + ' @ ' + kanal);
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    ime = rezultat.vzdevek;
    urediKanal();
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    kanal = rezultat.kanal;
    urediKanal()
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});